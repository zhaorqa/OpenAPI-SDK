package openapi4j.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;

import openapi4j.commons.TokenManager;
import openapi4j.exception.OpenAPIException;
import openapi4j.util.HttpUtil;

/**
 * Copyright(c) 2015-2015 by yonyouup. All Rights Reserved 部门信息
 * 
 * @author liujl
 * @version <类版本> , 2015年12月20日
 * @see <相关类/方法>
 * @since <产品/模块版本>
 */
public class DepartmentService extends BaseService {
	final static Logger logger = LogManager.getLogger(DepartmentService.class);

	public DepartmentService() {

		this.access_token = TokenManager.getToKenId();

	}

	public DepartmentService(String token) {
		this.access_token = token;
	}

	/**
	 * 通过部门id获取部门信息
	 * @param id 部门id
	 * @param to_account 数据提供方id
	 * @return
	 * @throws OpenAPIException
	 */
	public JSONObject get(String id, String to_account) throws OpenAPIException {
		JSONObject record;
		try {
			Map<String, String> paramMap = new HashMap();
			paramMap.put("to_account", to_account);
			paramMap.put("id", id);
			String url = this.createURL("department/get", paramMap);
			logger.debug(url);
			record = JSONObject.parseObject(HttpUtil.get(url));
		} catch (Exception e) {
			throw new OpenAPIException(e.getMessage(), e);
		}
		return record;
	}

	/**
	 * 批量获取部门档案
	 * @param paramMap
	 * @return
	 * @throws OpenAPIException
	 */
	public JSONObject batchGet(Map<String, String> paramMap) throws OpenAPIException {
		JSONObject record;
		try {
			String url = this.createURL("department/batch_get", paramMap);
			logger.debug(url);
			record = JSONObject.parseObject(HttpUtil.get(url));
		} catch (Exception e) {
			throw new OpenAPIException(e.getMessage(), e);
		}
		return record;
	}

}
