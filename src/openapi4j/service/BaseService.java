package openapi4j.service;

import java.io.Serializable;
import java.util.Map;
import java.util.Properties;

import openapi4j.util.PropUtil;

public class BaseService implements Serializable {

	private static final long serialVersionUID = -8280794717935626188L;

	protected String access_token;

	protected String createURL(String apiName, Map<String, String> paramMap) {

		Properties prop = PropUtil.getProperties("/config.properties");
		String baseURL = prop.getProperty("baseURL");
		String from_account = prop.getProperty("from_account");
		String app_key = prop.getProperty("app_key");

		StringBuffer url = new StringBuffer();
		url.append(baseURL);
		url.append(apiName);
		url.append("?from_account=" + from_account + "&");

		url.append("app_key=" + app_key + "&");
		url.append("token=" + access_token + "&");
		if (paramMap != null && paramMap.size() > 0) {
			for (String key : paramMap.keySet()) {
				String value = paramMap.get(key);
				url.append(key + "=" + value + "&");
			}

		}
		url.deleteCharAt(url.length() - 1);
		return url.toString();
	}

}
