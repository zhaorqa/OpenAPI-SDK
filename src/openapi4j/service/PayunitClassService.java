package openapi4j.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;

import openapi4j.commons.TokenManager;
import openapi4j.exception.OpenAPIException;
import openapi4j.util.HttpUtil;

/**
 * 交易单位分类信息
 */
public class PayunitClassService extends BaseService {
	final static Logger logger = LogManager.getLogger(PayunitClassService.class);

	public PayunitClassService() {
		this.access_token = TokenManager.getToKenId();
	}

	public PayunitClassService(String token) {
		this.access_token = token;
	}
	
	/**
	 * 获取单个交易单位分类信息
	 * @param id           交易单位分类信息 id
	 * @param to_account   目标企业code
	 * @return             交易单位分类信息
	 * @throws OpenAPIException
	 */
	public JSONObject get(String id, String to_account) throws OpenAPIException {
		JSONObject record;
		try {
			Map<String, String> paramMap = new HashMap();
			paramMap.put("to_account", to_account);
			paramMap.put("id", id);
			String url = this.createURL("payunitclass/get", paramMap);
			logger.debug(url);
			record = JSONObject.parseObject(HttpUtil.get(url));
		} catch (Exception e) {
			
			throw new OpenAPIException(e.getMessage(), e);
		}
		return record;
	}
	
	/**
	 * 批量获取交易单位分类信息
	 * @param paramMap  参数map
	 * @return
	 * @throws OpenAPIException
	 */
	public JSONObject batchGet(Map<String, String> paramMap) throws OpenAPIException {
		JSONObject record;
		try {
			String url = this.createURL("payunitclass/batch_get", paramMap);
			logger.debug(url);
			record = JSONObject.parseObject(HttpUtil.get(url));
		} catch (Exception e) {
			
			throw new OpenAPIException(e.getMessage(), e);
		}
		return record;
	}

}
